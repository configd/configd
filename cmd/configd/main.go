package main

import (
	"fmt"
	"log"

	"github.com/joho/godotenv"
)

func main() {
	fmt.Println("Welcome to configd")
	err := godotenv.Load()
	if err != nil {
		log.Fatal(err)
	}
}
